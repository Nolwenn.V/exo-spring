package co.simplon.promo16.exospring.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String showHomePage(Model model) {
        String message = "un message";
        model.addAttribute("message", message);
        Integer randomNumber = new Random().nextInt(10);
        model.addAttribute("randomNumber", randomNumber);
        return "index";
    }

    @GetMapping("/loop")
    public String showCatList(Model model) {
        List<String> catList = new ArrayList<>();
        catList.add("Bengal");
        catList.add("Maine coon");
        catList.add("British shorthair");

        model.addAttribute("catList", catList);
        return "loop";
    }

}
