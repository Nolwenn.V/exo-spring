package co.simplon.promo16.exospring.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import co.simplon.promo16.entity.PostIt;

@Controller
public class TodoController {
    private List<PostIt> list = new ArrayList<>(List.of(
            new PostIt("title 1", "content 1"),
            new PostIt("title 2", "content 2"),
            new PostIt("title 3", "content 3")));

    @GetMapping("/todo")
    public String showTodos(Model model) {
        model.addAttribute("list", list);
        model.addAttribute("postIt", new PostIt());
        return "todo";
    }

    @PostMapping("/todo")
    public String addTodo(@ModelAttribute PostIt postIt) {
        list.add(postIt);
        return "redirect:/todo";
    }

}
