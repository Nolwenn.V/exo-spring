package co.simplon.promo16.entity;

public class PostIt {
    private String title;
    private String content;

    public PostIt(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public PostIt() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
